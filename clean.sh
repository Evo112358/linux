#!/usr/bin/env sh

if [ "$(id -u)" -ne 0 ]
then
	echo "You need root priviliges to run this script"
	exit
fi

rm -rf /usr/src/linux
rm -rf /lib/firmware
echo '[32mRemoved source dirs [0m'
