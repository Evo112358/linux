#!/usr/bin/env sh

echo '[34mExtracting firmware [0m'
tar -xf firmware.tar.bz2 2> /dev/null
echo '[34mExtracting kernel, please wait... [0m'
tar -xf linux.tar.bz2 2> /dev/null
sh buildkernel.sh
