#!/usr/bin/env sh

if [ "$(id -u)" -ne 0 ]
then
	echo "You need root priviliges to run this script"
	exit
fi

kernel() {
	git pull

		make olddefconfig # make changes
		make -j$(nproc)
		make -j$(nproc) modules_install
		make -j$(nproc) headers_install
		#make -j$(nproc) bzImage
		make -j$(nproc) install
}

install() {
	# find distro name ##thankyou pfetch
	while IFS='=' read -r key val; do
	    case $key in
	        (PRETTY_NAME)
	            distro=$val
	        ;;
	    esac
	done < /etc/os-release
	distro=${distro##[\"\']}
	distro=${distro%%[\"\']}
	distro=${distro%% *}
	boot=/boot
	dir=/boot/efi/EFI/$distro

	if [ ! -e $dir ]
	then mkdir -p $dir
	fi

	# make backup using last good kernel
	DIFF=$(diff $boot/vmlinuz $dir/vmlinuz)
	if [ $? -eq 0 ]
	then echo '[31mKernel is unchanged [0m'
	else echo '[33mMaking a backup kernel [0m'
	mv $dir/System.map $dir/System.map.old
	mv $dir/vmlinuz $dir/vmlinuz.old
	mv $dir/config.gz $dir/config.gz.old
	fi
	# install new kernel
	mv $boot/System.map $dir
	mv $boot/vmlinuz $dir
	cp /proc/config.gz $dir
	echo '[32mSuccessfully installed new kernel [0m'
}

main() {
	kernel
	install
}

main "$@"
