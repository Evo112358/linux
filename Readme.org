* Evo112358's personal linux kernel build script.

	The config/firmware supplied works with my Thinkpad E14
	and is intended to efistub boot from /dev/nvme0n1p4.

* USE AT YOUR OWN RISK

	Firmware used:
	- intel/ibt-19-0-4.sfi
	- intel-ucode/06-8c-01
	- i915/tgl_dmc_ver2_12.bin
	- iwlwifi-QuZ-a0-hr-b0-63.ucode


	Note: buildkernel.sh assumes the kernel and firmware 
	included are inplace if the directories are not empty.
